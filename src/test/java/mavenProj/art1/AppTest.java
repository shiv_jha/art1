package mavenProj.art1;

import org.junit.Test;

import junit.framework.Assert;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
	@Test
	public void addTestPositive()
	{
			//System.out.println("test");
		App objApp=new App();
		int z=objApp.divide(20, 5);
		Assert.assertEquals(4, z);
		
	}
	
	@Test(expected=ArithmeticException.class)
	public void addTestNegative()
	{
		
		App objApp=new App();
		int z=objApp.divide(20, 0);
		//Assert.assertEquals(4, z);
		//
	}
	
	@Test
	public void mainTest()
	{
		App.main(null);
	
	}
}
